const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const User = db.user;
const Admin = db.admin;
const Database = db.database;
const sql = require('mssql');

verifyToken = async (req, res, next) => {
  if (!req.headers["authorization"]) {
    return res.status(403).send({
      message: "Unauthorized"
    });
  }
  const Authorization = req.headers['authorization'].split('Bearer ')[1] || null;
  if (Authorization) {
    const secretKey = config.secret;
    const verificationResponse = jwt.verify(Authorization, secretKey, async function (err, decoded) {
      if (err) {
        return res.status(401).json({
          message: "Unauthorized"
        });
      }
      const userId = decoded.id;

      let findUser = await User.findByPk(userId);
      if (req.headers.user_type && req.headers.user_type == 'super_admin') {
        findUser = await Admin.findByPk(userId);
      }
      if (findUser) {
        req.user = findUser;
        next();
      } else {
        return res.json({
          success: false,
          message: 'Unauthorized'
        })
      }
    });
  } else {
    return res.status(404).send({
      message: "Authentication token missing"
    });
  }
};


isAdmin = (req, res, next) => {
  if (!req.headers["authorization"]) {
    return res.status(403).send({
      message: "Unauthorized"
    });
  }
  const Authorization = req.headers['authorization'].split('Bearer ')[1] || null;
  if (Authorization) {
    const secretKey = config.secret;
    const verificationResponse = jwt.verify(Authorization, secretKey, async function (err, decoded) {
      if (err) {
        return res.status(401).json({
          message: "Unauthorized"
        });
      }
      const userId = decoded.id;

      let findUser = await User.findByPk(userId);

      if (findUser) {
        if (findUser.roleId) {
          req.user = findUser;
          next();
        } else {
          console.log('cominfjsdfnsdjkfndsfn')
          return res.json({
            success: false,
            message: 'Not allowed to perfom this action.'
          })
        }
      } else {
        return res.json({
          success: false,
          message: 'Unauthorized'
        })
      }
    });
  } else {
    return res.status(404).send({
      message: "Authentication token missing"
    });
  }
};

const authJwt = {
  verifyToken: verifyToken,
  isAdmin: isAdmin,
};
module.exports = authJwt;