const db = require("../models");
const config = require("../config/auth.config");
const commonService = require("../services/common.serivce");
const User = db.user;
const Role = db.role;
const Admin = db.admin;
const sequelize = db.sequelize;
const _ = require("lodash");

const Op = db.Sequelize.Op;
var mysql = require('mysql');
const sql = require("mssql");
var jwt = require("jsonwebtoken");

var bcrypt = require("bcryptjs");
const {
  body
} = require('express-validator/check');
const {
  validationResult
} = require('express-validator/check');


exports.validate = (method) => {
  switch (method) {
    case 'testDB': {
      return [
        body('db_ip', 'Database Ip is required.'),
        body('db_user', 'Database user should be max 255 character long.'),
      ]
    }
  }
}


exports.signup = async (req, res) => {
  var allD = req.body;


  // Check again duplicate entry
  var checkDuplicate = User.findAll({
    email: allD.email,
    db_ip: allD.db_ip,
    db_name: allD.db_name,
  })
  if (!(checkDuplicate)) {
    res.send({
      status: 'false',
      message: "User or Databse already exits!"
    });
  }


  // Save User to Database
  User.create({
      username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
      db_ip: allD.db_ip,
      db_name: allD.db_name,
      db_pass: allD.db_pass,
      db_user: allD.db_user,
    })
    .then(user => {
      console.log('allD.db_ip', allD.db_ip)

      // Make connection with the DB
      var con = mysql.createConnection({
        host: allD.db_ip,
        user: allD.db_user,
        password: allD.db_pass
      });

      con.connect(function (err) {
        if (err) {
          res.send({
            success: false,
            message: 'Database connection error!'
          });
        };
        con.query(`CREATE DATABASE  IF NOT EXISTS ${allD.db_name}`, function (err, result) {
          if (err) {
            res.send({
              success: false,
              message: err
            });
          }
          console.log("Database created");
        });
      });

      if (req.body.roles) {
        Role.findAll({
          where: {
            name: {
              [Op.or]: req.body.roles
            }
          }
        }).then(roles => {
          user.setRoles(roles).then(() => {
            res.send({
              message: "User registered successfully!"
            });
          });
        });
      } else {
        // user role = 1
        user.setRoles([1]).then(() => {
          res.send({
            message: "User registered successfully!"
          });
        });
      }
    })
    .catch(async err => {
      commonService.errorLog(err, "app/controllers/auth.controller.js");
      res.status(500).send({
        message: 'Something went wrong!'
      });
    });
};

exports.signin = async (req, res) => {
  try {


    const dbData = await dbs.findOne({
      where: {
        id: req.body.db_id
      }
    })
    if (dbData && dbData.dataValues) {
      const msSqlConfig = {
        server: dbData.dataValues.db_ip,
        user: dbData.dataValues.db_user,
        password: dbData.dataValues.db_pass,
        database: dbData.dataValues.db_name,
        // port: 1434,
        options: {
          "enableArithAbort": true,
          "encrypt": false
        }
      }
      const msSql = await sql.connect(msSqlConfig);

      // Check DB already exits or not
      const checkDb = await msSql.query(`SELECT * FROM tblUsers WHERE UserName = '${req.body.username}' and UserPassword = '${req.body.password}' `);

      // Close connection
      msSql.close();
      if (checkDb && checkDb.recordset && checkDb.recordset[0]) {
        var token = jwt.sign({
          id: checkDb.recordset[0].UserID,
          db_id: req.body.db_id
        }, config.secret, {
          expiresIn: 86400 // 24 hours
        });
        res.status(200).json({
          success: true,
          token: token,
          data: {
            id: checkDb.recordset[0],
            roles: [],
          }
        });
        // var authorities = [];
        // user.getRoles().then(roles => {
        //   for (let i = 0; i < roles.length; i++) {
        //     authorities.push("ROLE_" + roles[i].name.toUpperCase());
        //   }
        //   res.status(200).json({
        //     success:true,
        //     data:{
        //       id: user.id,
        //       firstName: user.firstName,
        //       lastName: user.lastName,
        //       email: user.email,
        //       roles: authorities,
        //       accessToken: token
        //     }
        //   });
        // });
      } else {
        res.json({
          success: false,
          message: 'No user found'
        })
      }

    } else {
      res.json({
        success: false,
        message: 'No database found!'
      })
    }

  } catch (err) {
    commonService.errorLog(err, "app/controllers/auth.controller.js");
    res.json({
      success: false,
      message: 'Something went wrong!'
    })
  }

  // User.findOne({
  //     where: {
  //       email: req.body.email
  //     }
  //   })
  //   .then(async user => {
  //     if (!user) {
  //       return res.status(404).json({
  //         success:false,
  //         message: "User Not found."
  //       });
  //     }


  //     var passwordIsValid = bcrypt.compareSync(
  //       req.body.password,
  //       user.password
  //     );

  //     if (!passwordIsValid) {
  //       return res.status(401).json({
  //         success:false,
  //         message: "Invalid Password!"
  //       });
  //     }

  //     var token = jwt.sign({
  //       id: user.id
  //     }, config.secret, {
  //       expiresIn: 86400 // 24 hours
  //     });

  //     var authorities = [];
  //     user.getRoles().then(roles => {
  //       for (let i = 0; i < roles.length; i++) {
  //         authorities.push("ROLE_" + roles[i].name.toUpperCase());
  //       }
  //       res.status(200).json({
  //         success:true,
  //         data:{
  //           id: user.id,
  //           firstName: user.firstName,
  //           lastName: user.lastName,
  //           email: user.email,
  //           roles: authorities,
  //           accessToken: token
  //         }
  //       });
  //     });
  //   })
  //   .catch(err => {
  //     res.status(500).json({
  //       success:false,
  //       message: err.message
  //     });
  //   });
};

// API call for login and return DB list
exports.userLogin = async (req, res) => {
  try {
    const {
      email,
      password
    } = req.body;

    if (!email || !password) {
      return res.json({
        success: false,
        message: 'Email and Password are required.'
      })
    }

    User.findOne({
        attributes: ['id', 'email', 'roleId', 'firstName', 'lastName', 'account_verified', 'createdAt', 'updatedAt','password'],
        where: {
          email: email,
        }
      })
      .then(async data => {
        if (data) {
          bcrypt.compare(password, data.password, async function (err, result) {
            if (result) {
              var token = jwt.sign({
                id: data.id
              }, config.secret, {
                expiresIn: 86400 // 24 hours
              });

              if (data.roleId) {
                res.json({
                  success: true,
                  message: "Login successfully",
                  token: token,
                  data: data,
                });
              }

              if (!data.account_verified) {
                return res.json({
                  success: false,
                  message: 'Your E-mail is not verified.'
                })
              }
              // get all Databases
              const databases = await sequelize.query(`SELECT * FROM db_ids left join dbs on dbs.id = db_ids.db_id WHERE db_ids.user_id=${ data.id} group by db_ids.db_id;`, {
                type: sequelize.QueryTypes.SELECT
              });

              if (_.isEmpty(databases)) {
                res.json({
                  success: false,
                  message: "You are not assigned to any Database. So please contact to Admin.",
                });
              }
              delete data.dataValues.password;

              return res.json({
                success: true,
                message: "Login successfully",
                token: token,
                data: data,
                databases
              })
            } else {
              return res.json({
                success: false,
                message: 'Your password is incorrect!'
              })
            }
          });
        } else {
          return res.json({
            success: false,
            message: 'No data found!'
          })

        }
      })
      .catch(async err => {
        commonService.errorLog(err, "app/controllers/auth.controller.js");
        res.status(500).send({
          message: 'Something went wrong!'
        });
      });
  } catch (error) {
    commonService.errorLog(error, "app/controllers/auth.controller.js");
    return res.json({
      success: false,
      message: 'Something went wrong!'
    })
  }
}

exports.login = async (req, res) => {

  Admin.findOne({
      where: {
        email: req.body.email
      }
    })
    .then(user => {
      if (!user) {
        return res.status(404).json({
          success: false,
          message: "User not found!"
        });
      }
      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );
      if (!passwordIsValid) {
        return res.status(401).json({
          success: false,
          message: "Invalid Password!"
        });
      }

      var token = jwt.sign({
        id: user.id
      }, config.secret, {
        expiresIn: 86400 // 24 hours
      });


      res.status(200).json({
        success: true,
        data: {
          id: user.id,
          email: user.email,
          user_type: 'super_admin',
        },
        token: token
      });
    })
};
