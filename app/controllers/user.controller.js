const db = require("../models"); // models path depend on your structure
const userService = require("../services/user.serivce");
const commonService = require("../services/common.serivce");
const moment = require("moment");
const _ = require("lodash");
const bcrypt = require("bcryptjs");
const sql = require("mssql");
const User = db.user;
const {
  body
} = require('express-validator/check');
const {
  validationResult
} = require('express-validator/check');

exports.validate = (method) => {
  switch (method) {
    case 'createUser': {
      return [
        // body('fTitle', 'Title should be max 10 characters long').isLength({
        //     max: 10
        // }),
        // body('fAddress', 'Address should be max 255 characters long').isLength({
        //     max: 255
        // }),
        // body('FirstName', 'First name should be max 255 characters long').isLength({
        //     max: 255
        // }),
        // body('fLastName', 'Last name should be max 255 characters long').isLength({
        //     max: 255
        // }),
        // body('fBACSRef', 'fBACSRef should be max 10 characters long').isLength({
        //     max: 10
        // }),
        // body('PostCode', 'Postcode should be max 10 characters long').isLength({
        //     max: 10
        // }),
        // body('PassportNo', 'PassportNo should be max 20 characters long').isLength({
        //     max: 20
        // }),
        // body('NI', 'NI should be max 10 characters long').isLength({
        //     max: 10
        // }),
        // body('BonusCurrency', 'Bonus currency should be max 20 characters long').isLength({
        //     max: 20
        // }),
        // body('WageCurrency', 'Bonus currency should be max 20 characters long').isLength({
        //     max: 20
        // }),
        // body('Email', 'Email is not valid').isEmail()

      ]
    }
  }
}


exports.verifyToken = (req, res) => {
  User.findOne({
      where: {
        token: req.params.token,
      },
    })
    .then(async (user) => {
      if (user) {
        if (user.account_verified) {
          return res.json({
            message: "User is already verified.",
            success: true
          });
        }
        await User.update({
          account_verified: true
        }, {
          where: {
            id: user.id
          },
        });
        return res.json({
          message: "Thank you for confirming your account. Now login with your credentials.",
          success: true
        });

      }
      return res.json({
        message: "No data found.",
        success: false,
      });
    })
    .catch(async (err) => {
      commonService.errorLog(err, "app/controllers/user.controller.js");
      res.status(500).json({
        message: 'Something went wrong!',
        success: false,
      });
    });
  return;
};

// API for Forget Password
exports.forgetPassword = (req, res) => {

  User.findOne({
      where: {
        email: req.body.email,
      }
    })
    .then(async (user) => {
      if (!user) {
        return res.send({
          message: "No data Found",
          success: false
        })
      }

      let token = await userService.getRandomToken(999999);
      let data = {
        token: token,
        expiredAt: parseInt(moment().add(5, "minutes").format("X"))
      }
      var user = await User.update(data, {
        where: {
          email: req.body.email,
        }
      });

      return res.send({
        message: "We have send you mail for reset password.",
        success: true
      });
    })
    .catch(async (err) => {
      commonService.errorLog(err, "app/controllers/user.controller.js");
      res.status(500).send({
        message: 'Something went wrong!',
        success: false,
      });
    });
};

//API for Reset Password
exports.resetPassword = async (req, res) => {
  try {
    const checkToken = await userService.checkAndCreateToken(
      req.body.token
    )
    if (checkToken.success) {
      let data = {
        password: bcrypt.hashSync(req.body.password, 8),
        expiredAt: parseInt(moment().format("X"))
      }
      await User.update(data, {
        where: {
          email: checkToken.data,
        }
      })
      return res.json({
        message: 'Your password has successfully changed.',
        success: true
      });
    }
    return res.json({
      message: checkToken.message,
      success: false
    });
  } catch (err) {
    commonService.errorLog(err, "app/controllers/user.controller.js");
    res.status(500).json({
      message: 'Something went wrong!',
      success: false,
    });
  }
}
