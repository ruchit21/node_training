const db = require("../models");
const dbs = db.database;
const userDb = db.user_db;
const dbId = db.dbIds;
const Error_log = db.error_log;
const Connection_log = db.connection_log;

module.exports = {
  /**
   * Set pagination
   * @function getPaginationData
   * @param {number} { currentPage, perPage, total_record}
   * @returns {number} {per_page, page, total_pages, prev_enable, next_enable, start_from}
   */
  getPaginationData: async function (currentPage, perPage, total_record) {
    let per_page = perPage ? parseInt(perPage) : 20;
    let page = currentPage ? parseInt(currentPage) : 1;

    let total_pages = Math.ceil(total_record / per_page);
    let prev_enable = parseInt(page) - 1;
    let next_enable = total_pages <= page ? 0 : 1;

    let start_from = (page - 1) * per_page;
    let last_to = parseInt(start_from) + parseInt(per_page);
    last_to = last_to > total_record ? total_record : last_to;

    return {
      per_page,
      page,
      total_pages,
      prev_enable,
      next_enable,
      start_from
    };
  },

  // Check that db is connected with user or not
  checkDatabase: async function (headersData, uId) {
    try {
      if (headersData && headersData.db_id) {
        const dbData = await dbs.findOne({
          where: {
            id: headersData.db_id
          }
        });
        if (!dbData) {
          return {
            success: false,
            message: 'No databse found!'
          };
        } else {
          // Check DB connection
          const check = await dbId.findOne({
            where: {
              db_id: dbData.id,
              user_id: uId
            }
          });

          if (check) {
            return {
              success: true,
              data: dbData
            }
          } else {
            return {
              success: false,
              message: 'Your connection with this Database is not established.'
            }
          }
        }
      } else {
        return {
          success : false,
          message: 'Database id or user id not found'
        }
      }

    } catch (error) {
      this.errorLog(error, "app/services/common.service.js");
      return {
        success: false,
        message: 'Something went wrong!'
      }
    }
  },

  /**
   * Common function for create Error log in DB
   * @function errorLog
   * @param {object} value Error data
   */
  errorLog: async function (value, file) {
    return new Promise(async (resolve, reject) => {
      try {
        let obj = {
          error_name: value.name ? value.name : '',
          error_message: value.message ? value.message : '',
          error_path: value.stack ? value.stack : value,
          created_at: db.sequelize.literal('CURRENT_TIMESTAMP'),
          error_file: file ? file : '',
        }
        await Error_log.create(obj);
      } catch (error) {
        resolve();
      }
    });
  },



  /**
   * Common function for create mysql connection log in DB
   * @function connectionLog
   * @param {object} value Connection data
   */
   connectionLog: async function (value) {
    return new Promise(async (resolve, reject) => {
      try {
        await Connection_log.create(value);
      } catch (error) {
        this.errorLog(error, "app/services/common.service.js");
        resolve();
      }
    });
  },
};