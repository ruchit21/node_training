var mysql = require('mysql');
const config = require("../config/db.config.js");
const commonService = require("../services/common.serivce");
const md5 = require('md5');
const moment = require("moment");
const Sequelize = require("sequelize");
const db = require('../models/index.js');
const User = db.user;


module.exports = {

  createDatabase: async function () {
    return 'hello';
  },

  connectDB: async function (database, uname, password, host) {
    const sequelize = new Sequelize(
      database,
      uname,
      password, {
        host: host,
        dialect: config.dialect,
        operatorsAliases: false,
        pool: {
          max: config.pool.max,
          min: config.pool.min,
          acquire: config.pool.acquire,
          idle: config.pool.idle
        }
      }
    );
    sequelize
      .authenticate()
      .then(resp => {
        return sequelize;
      })
      .catch(e => e.message);
    return sequelize;
  },

  /**
   * Genrate random token
   * @function getRandomToken
   * @param {number} max 
   * @returns {number}
   */
  getRandomToken: async function (max) {
    return md5(Math.floor(Math.random() * Math.floor(max)));
  },

  checkAndCreateToken: async function (token) {
    //Find token
    return new Promise(async (resolve, reject) => {
      User.findOne({
          where: {
            token: token,
          },
        })
        .then((user) => {
          if (user) {
            if (user.expiredAt && parseInt(user.expiredAt) >= parseInt(moment().format('X'))) {
              resolve({
                data: user.email,
                success: true
              });
            } else {
              resolve({
                message: "The link is expired.",
                success: false
              });
            }
          }
          resolve({
            message: "Invalid. Please try again.",
            success: false,
          });
        })
        .catch(async(err) => {
          await commonService.errorLog(err, "app/services/user.service.js");
          resolve({
            message: err.message,
            success: false,
          });
        });
      return;
    });
  }
}