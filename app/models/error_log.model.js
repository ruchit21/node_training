module.exports = (sequelize, Sequelize) => {
  const Error_Log = sequelize.define("error_log", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      error_name: {
        type: Sequelize.STRING,
      },
      error_message: {
        type: Sequelize.STRING,
      },
      error_path: {
        type: Sequelize.STRING,
      },
      created_at: {
          type: Sequelize.DATE
      },
      error_file: {
        type: Sequelize.STRING
      }
    },   
    {
      underscored: true,
      timestamps: false, // disable the automatic adding of createdAt and    updatedAt columns
      freezeTableName: true
    });

    return Error_Log;
  };
  
  