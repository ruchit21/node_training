
module.exports = (sequelize, Sequelize) => {
    const Users = sequelize.define("user", {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        email: {
            allowNull: false,
            type: Sequelize.STRING(45),
        },
        password: {
            allowNull: false,
            type: Sequelize.STRING(255),
        },
        phoneNumber: {
            type: Sequelize.STRING(255),
        },
        roleId: {
            type: Sequelize.INTEGER,
        },
        firstName: {
            allowNull: false,
            type: Sequelize.STRING(255),
        },
        lastName: {
            allowNull: false,
            type: Sequelize.STRING(255),
        },
        account_verified: {
            type: Sequelize.BOOLEAN,
            defaultValue:0
        },
        token: {
            type: Sequelize.STRING(255),
        },
        expiredAt: {
            type: Sequelize.STRING(255)
        },
    });
    return Users;
};