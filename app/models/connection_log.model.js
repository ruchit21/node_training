module.exports = (sequelize, Sequelize) => {
  const Connection_Log = sequelize.define("connection_log", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      main_id: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      db_id: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      user_ip:{
        type: Sequelize.STRING,
        allowNull: true
      },
      mysql_config:{
        type: Sequelize.STRING,
        allowNull: true
      },
      created_at: {
          type: Sequelize.DATE
      }
    },   
    {
      underscored: true,
      timestamps: false, // disable the automatic adding of createdAt and    updatedAt columns
      freezeTableName: true
    });

    return Connection_Log;
  };
  
  