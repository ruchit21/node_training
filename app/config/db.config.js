module.exports = {
  HOST: "localhost",
  USER: "root",
  PASSWORD: "root",
  DB: "the_boss",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};

// module.exports = {
//   user:  "root",
//   password: "root",
//   server: 'DESKTOP-IST84CE', 
//   database: "AffordableFacilities",
//   options: {
//       "enableArithAbort": true,
//       "encrypt":false
//   }
// }

// const sqlConfig = {
//   user:  "root",
//   password: "root",
//   server: '192.168.0.200', 
//   database: "AffordableFacilities",
//   options: {
//       "enableArithAbort": true,
//       "encrypt":false
//   }
// };