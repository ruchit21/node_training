const controller = require("../controllers/user.controller");
const {
  authJwt
} = require("../middleware");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  // Api for User registration

  app.get("/api/user/verify-token/:token", controller.verifyToken);

  app.post("/api/user/forget-password", controller.forgetPassword);

  app.post("/api/user/reset-password", controller.resetPassword);

};